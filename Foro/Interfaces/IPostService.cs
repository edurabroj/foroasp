﻿using Foro.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foro
{
    public interface IPostService
    {
        List<Post> GetAll();
        List<Categoria> GetCategorias();
        void Add(Post model);
        void AddComment(Comentario model);
        List<Comentario> GetComments(int postId);
    }
}
