namespace Foro.Migrations
{
    using Models.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Foro.Contexts.ForoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Foro.Contexts.ForoContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Categorias.AddOrUpdate(
              p => p.Nombre,
              new Categoria { CategoriaId = 1, Nombre = "Ocio" },
              new Categoria { CategoriaId = 2, Nombre = "Educaci�n" }
            );

            context.Posts.AddOrUpdate(
              p => p.Contenido,
              new Post {
                  PostId=1,
                  Titulo = "Ocio en Cajamarca",
                  CategoriaId=1,
                  Contenido="Orlando Games y Billar"                  
              },
              new Post
              {
                  PostId = 2,
                  Titulo = "Educaci�n en Cuzco",
                  CategoriaId = 2,
                  Contenido = "Amigos, es bueno estudiar"
              }
            );

            context.Comentarios.AddOrUpdate(
              p => p.Texto,
              new Comentario
              {
                  ComentarioId=1,
                  PostId=1,
                  Texto="Tambi�n jugar f�tbol pe"
              },
              new Comentario
              {
                  ComentarioId = 2,
                  PostId = 1,
                  Texto = "<script>alert('hola')</script>"
              },
              new Comentario
              {
                  ComentarioId = 3,
                  PostId = 2,
                  Texto = "En youtebe hay tutoriales"
              },
              new Comentario
              {
                  ComentarioId = 4,
                  PostId = 2,
                  Texto = "<script>alert('educacion')</script>"
              }
            );
        }
    }
}
