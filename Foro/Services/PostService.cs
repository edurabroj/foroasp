﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Foro.Models.Entities;
using Foro.Contexts;

namespace Foro.Services
{
    public class PostService : IPostService
    {
        ForoContext db = new ForoContext();

        public void Add(Post model)
        {
            db.Posts.Add(model);
            db.SaveChanges();
        }

        public void AddComment(Comentario model)
        {
            db.Comentarios.Add(model);
            db.SaveChanges();
        }

        public List<Post> GetAll()
        {
            return db.Posts.Include("Categoria").ToList();
        }

        public List<Categoria> GetCategorias()
        {
            return db.Categorias.ToList();
        }

        public List<Comentario> GetComments(int postId)
        {
            return db.Comentarios.ToList().FindAll(c => c.PostId == postId);
        }
    }
}