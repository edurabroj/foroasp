﻿using Foro.Contexts;
using Foro.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foro.Controllers
{
    public class ForoController : Controller
    {
        IPostService _servicePost;

        public ForoController(IPostService servicePost)
        {
            _servicePost = servicePost;
        }
        
        // GET: Foro
        public ActionResult Index()
        {
            var posts = _servicePost.GetAll();
            return View(posts);
        }

        public ActionResult Create()
        {
            var categorias = _servicePost.GetCategorias();
            ViewBag.Categorias = new SelectList(categorias, "CategoriaId", "Nombre");

            return View();
        }

        [HttpPost]
        public ActionResult Create(Post model)
        {
            if (ModelState.IsValid)
            {
                _servicePost.Add(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult CreateComment(int postId)
        {
            return View(new Comentario { PostId=postId});
        }

        [HttpPost]
        public ActionResult CreateComment(Comentario model)
        {
            if (ModelState.IsValid)
            {
                _servicePost.AddComment(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult ViewComments(int postId)
        {
            return View(_servicePost.GetComments(postId));
        }
    }
}