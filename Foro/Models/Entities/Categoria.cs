﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Foro.Models.Entities
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        public string Nombre { get; set; }
    }
}