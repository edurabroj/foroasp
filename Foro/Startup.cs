﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Foro.Startup))]
namespace Foro
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
