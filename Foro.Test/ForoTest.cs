﻿using Foro.Controllers;
using Foro.Models.Entities;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foro.Test
{
    [TestFixture]
    class ForoTest
    {
        [Test]
        public void ListaPost()
        {
            var impFalsa = new Mock<IPostService>();

            impFalsa.Setup(o => o.GetAll()).Returns( //It.IsAny<Persona>()
                new List<Post>()
                {
                    new Post {
                        PostId=1,
                        Titulo = "Ocio en Cajamarca",
                        CategoriaId=1,
                        Contenido="Orlando Games y Billar"
                    },
                    new Post
                    {
                        PostId = 2,
                        Titulo = "Educación en Cuzco",
                        CategoriaId = 2,
                        Contenido = "Amigos, es bueno estudiar"
                    }
                }
            );

            var controller = new ForoController(impFalsa.Object);

            var resultado = controller.Index() as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Post>).Count());
        }

        [Test]
        public void GuardarExitoso()
        {
            var impFalsa = new Mock<IPostService>();

            var controller = new ForoController(impFalsa.Object);
            controller.Create(new Post());
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallido()
        {
            var impFalsa = new Mock<IPostService>();

            var controller = new ForoController(impFalsa.Object);
            controller.Create(new Post());
            controller.ViewData.ModelState.AddModelError("Titulo", "Nombre muy feo para el post");
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void Comentarios()
        {
            var impFalsa = new Mock<IPostService>();

            var controller = new ForoController(impFalsa.Object);
            impFalsa.Setup(o => o.GetComments(1)).Returns( //It.IsAny<Persona>()
                new List<Comentario>()
                {
                    
                }
            );
            var r = controller.ViewComments(1) as ViewResult;
            Assert.IsNotNull(r.Model);
        }

        [Test]
        public void CrearComentario()
        {
            var impFalsa = new Mock<IPostService>();

            var controller = new ForoController(impFalsa.Object);
            var c = new Comentario();
            var r = controller.CreateComment(c) as ViewResult;
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }
    }
}
